package com.rabobank.assignment.exception;

public class InvalidFileException extends Exception {

    public InvalidFileException() {}

    public InvalidFileException(final String message) {
        super(message);
    }
    public InvalidFileException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
