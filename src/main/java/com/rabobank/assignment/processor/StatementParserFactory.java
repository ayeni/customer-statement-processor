package com.rabobank.assignment.processor;

import com.rabobank.assignment.exception.InvalidFileException;

public class StatementParserFactory {

    public StatementParser createStatementParser(final StatementInputType inputType) throws InvalidFileException {

        if (inputType == StatementInputType.CSV) return new CsvStatementParser();
        else if (inputType == StatementInputType.XML) return new XmlStatementParser();
        else throw new InvalidFileException("Invalid input format: " + inputType.name());
    }
}
