package com.rabobank.assignment.processor;

public enum StatementInputType {
    CSV,
    XML
}
