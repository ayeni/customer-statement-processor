package com.rabobank.assignment.processor;

public interface StatementParser extends AutoCloseable {

    /**
     * Reads next record in a statements record file.
     * @return Next StatementRecord in the file. If no record exists to read anymore, null is returned.
     */
    StatementRecord readNext();

    /**
     * Sets the statements record file to be parsed.
     * @param pathToInputFile The statements record file to be parsed.
     * @throws Exception Throws exception if the file cannot be opened.
     */
    void setInputFile(final String pathToInputFile) throws Exception;
}
