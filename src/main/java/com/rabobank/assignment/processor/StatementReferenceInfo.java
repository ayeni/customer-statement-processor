package com.rabobank.assignment.processor;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class StatementReferenceInfo {

    private Long   transactionReference;
    private String description;
}
