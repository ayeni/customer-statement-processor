package com.rabobank.assignment.processor;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

@Slf4j
public class CsvStatementParser implements StatementParser {

    private BufferedReader statementBufferedReader;

    @Override
    public void setInputFile(final String pathToInputFile) throws Exception {

        statementBufferedReader = new BufferedReader(new FileReader(pathToInputFile));

        statementBufferedReader.readLine(); //First line is the header line, we just omit it.
    }

    @Override
    public StatementRecord readNext() {

        StatementRecord statementRecord = null;

        try {
            if (statementBufferedReader.ready()) {

                statementRecord = new StatementRecord();

                String line = statementBufferedReader.readLine();

                StringTokenizer tokens = new StringTokenizer(line, ",");

                try {
                    statementRecord.setTransactionReference(Long.parseLong(tokens.nextToken().trim()));
                    statementRecord.setAccountNumber(tokens.nextToken().trim());
                    statementRecord.setDescription(tokens.nextToken().trim());
                    statementRecord.setStartBalance(new BigDecimal(tokens.nextToken().trim()));
                    statementRecord.setMutation(new BigDecimal(tokens.nextToken().trim()));
                    statementRecord.setEndBalance(new BigDecimal(tokens.nextToken().trim()));
                } catch (NoSuchElementException e) {
                    log.error("Line in invalid format: " + line);
                }
            }
        } catch (IOException e) {
            log.error("Error: Cannot read the record.");
            return null;
        }

        return statementRecord;
    }

    @Override
    public void close() throws Exception {
        try {
            statementBufferedReader.close();
        } catch (IOException e) {
            log.error("Error closing the file: ", e);
            throw new IOException(e);
        }
    }
}
