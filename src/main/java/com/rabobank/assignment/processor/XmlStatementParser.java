package com.rabobank.assignment.processor;

import lombok.extern.slf4j.Slf4j;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.math.BigDecimal;

@Slf4j
public class XmlStatementParser implements StatementParser {

    private XMLEventReader reader;

    @Override
    public StatementRecord readNext() {

        StatementRecord statementRecord = null;

        try {
            while (reader.hasNext()) {
                XMLEvent nextEvent = reader.nextEvent();

                if (nextEvent.isStartElement()) {

                    StartElement startElement = nextEvent.asStartElement();
                    switch (startElement.getName().getLocalPart()) {
                        case "record":
                            if (statementRecord == null) {
                                statementRecord = new StatementRecord();
                            } else {
                                log.error("'record' element is found at unexpected location.");
                                return null;
                            }

                            Attribute referenceAttribute = startElement.getAttributeByName(new QName("reference"));
                            if (referenceAttribute != null) {
                                statementRecord.setTransactionReference(Long.parseLong(referenceAttribute.getValue().trim()));
                            }

                            break;
                        case "accountNumber":
                            nextEvent = reader.nextEvent();
                            statementRecord.setAccountNumber(nextEvent.asCharacters().getData());
                            break;
                        case "description":
                            nextEvent = reader.nextEvent();
                            statementRecord.setDescription(nextEvent.asCharacters().getData());
                            break;
                        case "startBalance":
                            nextEvent = reader.nextEvent();
                            statementRecord.setStartBalance(new BigDecimal(nextEvent.asCharacters().getData().trim()));
                            break;
                        case "mutation":
                            nextEvent = reader.nextEvent();
                            statementRecord.setMutation(new BigDecimal(nextEvent.asCharacters().getData().trim()));
                            break;
                        case "endBalance":
                            nextEvent = reader.nextEvent();
                            statementRecord.setEndBalance(new BigDecimal(nextEvent.asCharacters().getData().trim()));
                            break;
                    }
                } else if (nextEvent.isEndElement()) {
                    EndElement endElement = nextEvent.asEndElement();
                    if (endElement.getName().getLocalPart().equals("record")) {
                        return statementRecord;
                    }
                }
            }
        } catch (XMLStreamException e) {
            log.error("Error: Cannot read the XML file.");
            return null;
        }

        return statementRecord;
    }

    @Override
    public void close() throws Exception {
        try {
            reader.close();
        } catch (XMLStreamException e) {
            log.error("Error closing the XML reader: ", e.getMessage());
            throw new XMLStreamException(e);
        }
    }

    @Override
    public void setInputFile(final String pathToInputFile) throws Exception {
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        reader = xmlInputFactory.createXMLEventReader(new FileInputStream(pathToInputFile));
    }
}
