package com.rabobank.assignment;

import com.rabobank.assignment.exception.InvalidFileException;
import com.rabobank.assignment.processor.StatementEvaluator;
import com.rabobank.assignment.processor.StatementInputType;
import com.rabobank.assignment.processor.StatementParser;
import com.rabobank.assignment.processor.StatementParserFactory;
import com.rabobank.assignment.processor.StatementReferenceInfo;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * Rabobank customer statement processor application. Evaluates an input statement records file for validity and
 * reports invalid records. For evaluation of the records, correctness of end balance value and reference id
 * uniqueness are checked.
 *
 * @author Ayhan Yeni
 */
@Slf4j
public class CustomerStatementProcessor {

    public static void main(String[] args) {

        if (args.length > 0) {

            try {

                CustomerStatementProcessor customerStatementProcessor = new CustomerStatementProcessor();

                List<StatementReferenceInfo> failedStatementReferenceInfos = customerStatementProcessor.process(args[0]);

                customerStatementProcessor.printFailedStatementInfos(failedStatementReferenceInfos);
            } catch (InvalidFileException ife) {
                log.error(ife.getMessage());
            } catch (Exception e) {
                log.error(e.getMessage());
                e.printStackTrace();
            }
        } else {
            log.info("Error: Input file name parameter is missing.");
        }
    }

    /**
     * Processes a customer statement records file.
     *
     * @param inputFilePath Path to customer statement records file to be processed.
     *                      Filename can have only .xml or .csv extensions.
     * @return Number of failed records.
     * @throws InvalidFileException Throws InvalidFileException in case of invalid file extension.
     * @throws Exception Throws Exception in case of error opening or reading the input file.
     */
    public List<StatementReferenceInfo> process(final String inputFilePath) throws InvalidFileException, Exception {

        StatementInputType statementInputType;

        if (inputFilePath.toLowerCase().endsWith(".xml")) {
            statementInputType = StatementInputType.XML;
        } else if (inputFilePath.toLowerCase().endsWith(".csv")) {
            statementInputType = StatementInputType.CSV;
        } else {
            throw new InvalidFileException("Error: Invalid input file format. Expected formats are xml or csv.");
        }

        try (StatementParser statementParser = new StatementParserFactory().createStatementParser(statementInputType)) {
            statementParser.setInputFile(inputFilePath);
            List<StatementReferenceInfo> failedStatementReferenceInfos =
                    new StatementEvaluator(statementParser).evaluate();

            return failedStatementReferenceInfos;
        }
    }

    private void printFailedStatementInfos(List<StatementReferenceInfo> failedStatementReferenceInfos) {

        if (failedStatementReferenceInfos != null) {

            log.info("Invalid Transactions:");

            for (StatementReferenceInfo statementReferenceInfo : failedStatementReferenceInfos) {
                log.info(statementReferenceInfo.getTransactionReference() + " ...... " +
                        statementReferenceInfo.getDescription());
            }
        } else {
            log.info("No failed records to print.");
        }
    }
}
