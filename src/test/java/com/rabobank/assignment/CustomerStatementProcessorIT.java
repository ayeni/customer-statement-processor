package com.rabobank.assignment;

import com.rabobank.assignment.processor.StatementReferenceInfo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class CustomerStatementProcessorIT {

    private static final String CSV_INPUT_FILE          = "src/test/resources/test-data/records.csv";
    private static final String XML_INPUT_FILE          = "src/test/resources/test-data/records.xml";

    private static final Long   CSV_FAILING_REFERENCE   = 112806L;
    private static final Long   XML_FAILING_REFERENCE   = 145501L;

    @Test
    public void process_InputCSVWithFailures_Success() throws Exception {

        List<StatementReferenceInfo> failedRecords = new CustomerStatementProcessor().process(CSV_INPUT_FILE);

        Assertions.assertEquals(2, failedRecords.size());
        Assertions.assertTrue(failedRecords.stream().anyMatch(record -> record.getTransactionReference().equals(CSV_FAILING_REFERENCE)));
    }

    @Test
    public void process_InputXMLWithFailures_Success() throws Exception {

        List<StatementReferenceInfo> failedRecords = new CustomerStatementProcessor().process(XML_INPUT_FILE);

        Assertions.assertEquals(2, failedRecords.size());
        Assertions.assertTrue(failedRecords.stream().anyMatch(record -> record.getTransactionReference().equals(XML_FAILING_REFERENCE)));
    }
}
