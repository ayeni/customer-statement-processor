package com.rabobank.assignment.processor;

import java.util.List;

public class TestStatementParser implements StatementParser {

    private List<StatementRecord> testRecords;
    private int index = 0;

    @Override
    public void setInputFile(final String pathToInputFile) throws Exception {

    }

    @Override
    public StatementRecord readNext() {
        if (testRecords != null && index < testRecords.size())
            return testRecords.get(index++);
        else return null;
    }

    @Override
    public void close() throws Exception { }

    public void setTestRecords(List<StatementRecord> testRecords) {
        this.testRecords = testRecords;
    }
}
