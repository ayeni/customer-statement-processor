package com.rabobank.assignment.processor;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StatementParserTest {

    private static final String CSV_INPUT_FILE = "src/test/resources/test-data/records.csv";
    private static final String XML_INPUT_FILE = "src/test/resources/test-data/records.xml";

    @Test
    public void read_AllStatementsFromCsvInput_Success() throws Exception {

        try {
            StatementParser statementParser = new StatementParserFactory().createStatementParser(StatementInputType.CSV);
            statementParser.setInputFile(CSV_INPUT_FILE);

            int count = 0;

            StatementRecord statementRecord = statementParser.readNext();
            while (statementRecord != null) {
                count++;
                statementRecord = statementParser.readNext();
            }

            Assertions.assertEquals(10, count);
        } catch (Exception e) {
            Assertions.fail(e);
        }
    }

    @Test
    public void read_AllStatementsFromXmlInput_Success() throws Exception {

        try {
            StatementParser statementParser =
                new StatementParserFactory().createStatementParser(StatementInputType.XML);

            statementParser.setInputFile(XML_INPUT_FILE);

            int count = 0;

            StatementRecord statementRecord = statementParser.readNext();
            while (statementRecord != null) {
                statementRecord = statementParser.readNext();
                count++;
            }

            Assertions.assertEquals(10, count);
        } catch (Exception e) {
            Assertions.fail(e);
        }
    }
}
