# Rabobank Customer Statement Processor

Rabobank Customer Statement Processor application evaluates a customer statement records file for validity and reports invalid records. 
For evaluation of the records, correctness of end balance values and uniqueness of reference ids are checked.


Run the below command to start the application.

```sh
java -jar customer-statement-processor.jar <customer_statement_records_file>
```
The 'customer_statement_records_file' parameter is the path to the customer statement records file. 
The file name has to have '.csv' or '.xml' extension.

## Unit and Integration Tests
The integration and unit tests can be run separately. Use below commands from the project root directory to run the tests.

To run all tests:
```sh
mvn test 
```
To run only the unit tests:
```sh
mvn test -Dtest='**/*Test'
```
To run only the integration tests:
```sh
mvn test -Dtest='**/*IT'
```

## Design Criteria

* New file formats shall be introduced easily: The input files can be in two different formats and new format 
  types can be introduced in the future. So, factory method design pattern is used to create and use the correct 
  file parser during the process run.
* Very big customer statement files shall be supported: Monthly customer statement records of Rabobank could be too big.
  Reading all the records in the input file to the memory and run the validity check could limit the maximum 
  size of input files the application can process. So, it is preferred to read and evaluate the records in the input files 
  in a streamed way. Reading the records in '.csv' files line by line in a streamed way is achieved using FileReader 
  class of Java. To achieve this behaviour in '.xml' files, StAX XML parser is used to parse and read records. 
  Since, DOM parser reads all the file to the memory at once and SAX parser doesn't allow reading each record and 
  processing it, they are not used for XML parsing. This approach allows the application to be able to process 
  much bigger customer statement files.   